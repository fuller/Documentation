---
eleventyNavigation:
  key: TablesInMarkdown
  title: Tables in Markdown
  parent: Markdown
  order: 70
---

Markdown Articles can contain tables to structure presented data.

## table syntax

Markdown tables are written ("drawn") using the characters pipe `|`, dash `-` and colon `:`.

A simple table looks like this

```
| This   | is    | a       |
| ---    | ---   | ---     |
| simple | table | example |
```

| This   | is    | a       |
| ---    | ---   | ---     |
| simple | table | example |

The table columns do not have to align in the un-rendered text, but it improves readability to keep everything aligned
in the un-rendered form as well.

Some editors align the table structure automatically.

The first line a table forms the head of the table. It is separated from the rest of the data by a line containing dashes.

```
| Name   | Comment                                                                 |
|:-------|:------------------------------------------------------------------------|
| Alice  | Always involved in various communications                               |
| Bob    | A good guy, who likes to communicate with Alice                         |
| Malroy | Not so nice guy. Tries to mess with the communication of Alice and Bob. |
```

| Name   | Comment                                                                 |
|:-------|:------------------------------------------------------------------------|
| Alice  | Always involved in various communications                               |
| Bob    | A good guy, who likes to communicate with Alice                         |
| Malroy | Not so nice guy. Tries to mess with the communication of Alice and Bob. |

The line following the header line may contain a formatting help to the renderer.

It depends on the place of the colon `:` (if any) how the table is rendered.

If the colon is to the left of the line of dashes separating data from the header, the data is rendered in a left-aligned
form.

For example

```
| Left oriented rendering |
|:------------------------|
| 150.0                   |
| or text                 |
```

Renders as:

| Left oriented rendering |
|:------------------------|
| 150.0                   |
| or text                 |

Whereas:

```shell
| Right oriented rendering |
|-------------------------:|
| 150.0                    |
| or text                  |
```

is rendered as

| Right oriented rendering |
|-------------------------:|
| 150.0                    |
| or text                  |

If the rendering hint is placed on both sides of the dashed line, the data is rendered as centered:

```
| Centerd rendering |
|:-----------------:|
| 150.0             |
| or text           |
```

Is rendered as:

| Centerd rendering |
|:-----------------:|
| 150.0             |
| or text           |

Providing no rendering hint leaves it to the renderer to decide how to render the data. Left-aligned is a common default.

```shell
| Un-hinted rendering |
|---------------------|
| 150.0               |
| or text             |
```

Is rendered as:

| Un-hinted rendering |
|---------------------|
| 150.0               |
| or text             |


## Table variations

Some renderers allow to omit the delimiting pipe symbols `|` at the side of the table:

```
This   | is    | a      
---    | ---   | ---    
simple | table | example
```

Is rendered as:

This   | is    | a
---    | ---   | ---    
simple | table | example

This is even considered an error by some editors.

However, for readability reasons we propose to use the delimited form within Codeberg.
