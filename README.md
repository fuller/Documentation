# Codeberg Documentation
This repository contains the documentation for Codeberg, with some code to build it into
a static website.

Please have a look into it and consider to help writing the Documentation. This is still very much work-in-progress, the more useful material we collect, the better we can later present it! All contributions are very welcome!

For an introduction on contributing to Codeberg Documentation, please have a look
at [the section on improving the documentation](https://docs.codeberg.org/improving-documentation/).


## Usage
### Local Development
If you want to work on the documentation, for example by writing your own articles,
the easiest way to do so is to fork the documentation repository and develop locally.

First, run

```npm install```

to install all dependencies (they will be installed only for this project, not globally).
You only have to do this once.

Then run

```npm run serve```

to start a development web server that by default is listening at `http://localhost:8080`.

Now you can simply change, add or remove files, save them and the development server
should automatically reload all changed pages using the amazing Browsersync.

When you're done, commit your changes to your fork and write a pull request for
Codeberg/Documentation. We're happy about every contribution!

### Local development using the Dockerfile

If you do not have nodejs installed or do not want to run the development web server directly on your host,
you can also use a docker container.

You must have a container-engine installed (docker, podman, etc.)

First build the container image:

```shell
docker build -t codeberg/documentation-server .
```

You do not have to rebuild the image every time. Once you build the image you can always start the development
webserver using the container engine:

```shell
docker run --rm -v $PWD:/opt/documentation codeberg/documentation-server:latest
```

Use the "External" URL the container outputs on startup to access your documentation.

Changes to files in the documentation are reflected in the browser as the server regularly updates the generated files.

Use `Ctrl-C` to exit / end the container.

The parameters are:

`--rm` removes the container after it's use
`-v` mounts the current (documentation repository root) folder to `/opt/documentation` in the container.

`codeberg/documentation-server:latest` refers to the container image built in the first step (using `docker build`).

### Build & Deployment
Like for local development, before building and deploying you first have to install
the dependencies (once):

```npm install```

To build the entire website to the `_site` directory run

```npm run build```

Instead, to directly publish the page to Codeberg pages, you can also run

```npm run deploy```

which includes a call to `npm run build`.

### Technical Information
This website uses [Eleventy](https://www.11ty.dev/), a static site generator.

It's supplied as a dev-dependency in `package.json` and its dependencies are locked
with `package-lock.json` to try to ensure reproducible builds.


## License and Contributors
This website (excluding bundled fonts) is licensed under CC BY-SA 4.0. See the [LICENSE](LICENSE.md) file for details.

The contributors are listed in `CONTRIBUTORS.md`.